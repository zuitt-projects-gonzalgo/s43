/*const addPost = document.querySelector('#form-add-post');
const title = document.querySelector('#txt-title');
const body = document.querySelector('#txt-body');
const editPost = document.querySelector('#form-edit-post');
const editId = document.querySelector('#txt-edit-id');
const editTitle = document.querySelector('#txt-edit-title');
const editBody = document.querySelector('#txt-edit-body');
const Post = document.querySelector('#div-post-entries');*/

let posts = [];
let count = 1;

document.querySelector("#form-add-post").addEventListener("submit", (e) => {
	
	e.preventDefault();
	
	posts.push({
		id : count,
		title : document.querySelector("#txt-title").value,
		body : document.querySelector("#txt-body").value
	})

	count++

	showPosts(posts);

});

const showPosts = (posts) => {
	
	let postEntries = '';
	
	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">

				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>

				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	})

	document.querySelector("#div-post-entries").innerHTML = postEntries;
};

//Edit Post function

const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML;

	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	
	document.querySelector("#txt-edit-title").value = title;

	document.querySelector("#txt-edit-body").value = body;

};

//Update post

document.querySelector("#form-edit-post").addEventListener("submit", (e) => {

	e.preventDefault();

	for(let i = 0; i < posts.length; i++) {
		if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value) {
			posts[i].title = document.querySelector("#txt-edit-title").value;
			posts[i].body = document.querySelector("#txt-edit-body").value;

			showPosts(posts)

			break;
		}
	}
});

// A C T I V I T Y

//delete post
const deletePost = (id) => {

	let post = document.querySelector(`#post-${id}`);
	post.remove();
};